package com.example.familymoney;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String LOG_TAG = "MyLog";
    TextView label_d;
    Button btn_d;
    TextView label_r;
    Button btn_r;
    TextView label_summa;
    String summa;
    String num_1;
    String num_2;
    SharedPreferences sPref;

    final String SAVED_TEXT = "saved_text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        label_d = (TextView) findViewById(R.id.label_d);
        btn_d = (Button) findViewById(R.id.btn_d);
        btn_d.setOnClickListener(this);
        label_r = (TextView) findViewById(R.id.label_r);
        btn_r = (Button) findViewById(R.id.btn_r);
        btn_r.setOnClickListener(this);
        label_summa = (TextView)findViewById(R.id.text_summ);
    }


    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btn_d){
            if (label_summa.getText().toString().isEmpty()){
                label_summa.setText("0");
            }
            num_1 = label_d.getText().toString();
            int num_one = Integer.parseInt(num_1);
            num_2 = label_summa.getText().toString();
            int num_two = Integer.parseInt(num_2);
            int SUMM = num_one + num_two;
            summa = ""+SUMM;
            label_summa.setText(summa);
            label_d.setText("");
            Log.d(LOG_TAG, "Добавление числа");
        }else if (v.getId()==R.id.btn_r){
            if (label_summa.getText().toString().isEmpty()){
                label_summa.setText("0");
            }
            int min_one = Integer.parseInt(label_summa.getText().toString());
            int min_two = Integer.parseInt(label_r.getText().toString());
            int min_SUM = min_one - min_two;
            label_summa.setText(min_SUM + "");
            label_r.setText("");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (label_summa.getText().toString().isEmpty()){
            label_summa.setText("0");
        }else {
            loadText();
        }
        Log.d(LOG_TAG, "onStart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveText();
        Log.d(LOG_TAG, "onDestroy");
    }

    void saveText() {
        sPref = getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(SAVED_TEXT, label_summa.getText().toString());
        ed.commit();
        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
    }
    void loadText() {
        //sPref = getPreferences(MODE_PRIVATE);
        sPref = getSharedPreferences("MyPref", MODE_PRIVATE);
        String savedText = sPref.getString(SAVED_TEXT, "");
        label_summa.setText(savedText);
        Toast.makeText(this, "Data loaded", Toast.LENGTH_SHORT).show();
    }
}
